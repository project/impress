<?php
/**
 * @file
 * impress.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function impress_taxonomy_default_vocabularies() {
  return array(
    'impress_slideshow' => array(
      'name' => 'Impress Slideshow',
      'machine_name' => 'impress_slideshow',
      'description' => 'Used to group impress slides content into separate slideshows.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
